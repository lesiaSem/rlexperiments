# README #

Experiments for 590.02 class on RL.

I would like to express gratitude for the publically available code and data used in the experiments to:

Lantao Yu: https://github.com/LantaoYu/SeqGAN

Andrej Karpathy: https://github.com/karpathy/neuraltalk2

Bo Dai: https://github.com/doubledaibo/gancaption_iccv2017

Xinpeng Chen: https://github.com/chenxinpeng/Optimization_of_image_description_metrics_using_policy_gradient_methods
